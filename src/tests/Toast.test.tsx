import React from 'react';
import { fireEvent, getByTestId, render, screen } from '@testing-library/react';
import jest from 'jest-mock'

import Toast from "../stories/Toast"


//eslint-disable-line
test('renders toast component', () => { 
  render(<Toast />);
});

test('handleActionClick is called when button is clicked', () => {
  // Mock the handleActionClick function
  const handleActionClick = jest.fn();

  render(
    <Toast
      message="This is a test message"
      onClose={() => {}}
      onActionClick={handleActionClick}
      toastType="success"
      actionButtonText="Take Action"
      toastHeaderText="Header"
      size="large"
    />
  );

  // Find the button by its role and text content
  const actionButton = screen.getByRole('button', { name: /Take Action/ });

  // Simulate a click event on the button
  fireEvent.click(actionButton);

  // Check if handleActionClick is called once
  expect(handleActionClick).toHaveBeenCalledTimes(1);
});
