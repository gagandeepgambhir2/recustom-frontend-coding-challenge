import React from "react";
import type { Meta, StoryObj } from "@storybook/react";

import Toast, { ToastProps } from "./Toast";

const meta = {
  title: "Example/Toast",
  component: Toast,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
  argTypes: {
    // message: { control: 'Test Message' },
    toastType: { control: "radio" },
    size: { control: "radio" },
  },
} satisfies Meta<typeof Toast>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Success: Story = {
  args: {
    duration: 500000000000,
    toastType: "success",
    message:
      "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
    actionButtonText: "Undo",
    onActionClick: () => alert("Undo Clicked"),
  },
};

export const Warning: Story = {
  args: {
    duration: 500000000000,
    toastType: "warning",
    message:
      "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",

    onActionClick: () => alert("Undo Clicked"),
    actionButtonText: "Undo",
  },
};

export const Custom: Story = {
  args: {
    duration: 500000000000,
    toastType: "custom",
    message:
      "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
    actionButtonText: "Undo",
    onActionClick: () => alert("Undo Clicked"),
  },
};
