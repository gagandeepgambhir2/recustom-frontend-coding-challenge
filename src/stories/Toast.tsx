// components/Toast.tsx
import React, { useState, useEffect } from "react";
// import successIcon from './assets/success.svg';
import successIcon from "./assets/success.svg";
import warningIcon from "./assets/warning.svg";
import greyCross from "./assets/greyCross.svg";
import greenCross from "./assets/greenCross.svg";
import redCross from "./assets/redCross.svg";
import avatarIcon from "./assets/Avatar.svg";
import Image from "next/image";

import "./toast.css";
export interface ToastProps {
  message: string;
  duration?: number;
  onClose: () => void;
  onActionClick?: () => void;
  toastType?: "success" | "warning" | "custom";
//   width?: string;
  actionButtonText?: string;
  toastHeaderText?: string;
  toastIcon?: string;
  size?: "small" | "large";
}

const Toast: React.FC<ToastProps> = ({
  duration = 5000 ,

  message,
  onActionClick,
  toastType = "success",
//   width = "500px",
  actionButtonText,
  toastHeaderText,
  toastIcon,
  size = "large",
}) => {
  const [visible, setVisible] = useState(true);
  const [timer, setTimer] = useState<any>(null);

  function onClose() {
    setVisible(false);
    clearTimeout(timer);
  }

  useEffect(() => {
    const id = setTimeout(() => {
      setVisible(false);
    }, duration);
    setTimer(id);
    return () => clearTimeout(id);
  }, [duration]);

  const handleActionClick = () => {
    onActionClick && onActionClick();
    onClose();
  };

  return (
    <div
      className={`toast ${visible ? "show" : ""}  ${toastType}`}
      style={{
        width: size==="small" ? "300px" : "500px",
      }}
    >
      {toastType === "success" && (
        <div>
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-flex align-items-center success-header">
              <Image
                src={successIcon}
                alt="success"
                width={17}
                height={17}
                className="mr-2"
              />
              {toastHeaderText ?? "Success"}
            </div>

            <div>
              <Image
                src={greenCross}
                alt="success"
                width={12}
                height={12}
                className="mr-2"
                onClick={onClose}
              />
            </div>
          </div>
          <div className="success-message">{message}</div>

          <button className="success-button" type="button" onClick={handleActionClick}>
            {actionButtonText ?? "Take Action"}
          </button>
        </div>
      )}
      {toastType === "warning" && (
        <div>
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-flex align-items-center warning-header">
              <Image
                src={warningIcon}
                alt="warning"
                width={17}
                height={17}
                className="mr-2"
              />
              {toastHeaderText ?? "Attention"}
            </div>

            <div>
              <Image
                src={redCross}
                alt="warning"
                width={12}
                height={12}
                className="mr-2"
                onClick={onClose}
              />
            </div>
          </div>
          <div className="warning-message">{message}</div>

          <button className="warning-button" onClick={handleActionClick}>
            {actionButtonText ?? "Take Action"}
          </button>
        </div>
      )}
      {toastType === "custom" && (
        <div>
          <div className="d-flex align-items-start justify-content-between">
            <div className="d-flex align-items-start">
              <Image
                src={toastIcon ?? avatarIcon}
                alt="warning"
                width={32}
                height={32}
                className="mr-2 rounded-full"
              />
              <div>
                <div className="d-flex align-items-center custom-header">
                  {toastHeaderText ?? "Marry Hulya"}
                </div>
                <div className="custom-message">{message}</div>

                <button className="custom-button" onClick={handleActionClick}>
                  {actionButtonText ?? "Take Action"}
                </button>
              </div>
            </div>

            <Image
              src={greyCross}
              alt="warning"
              width={12}
              height={12}
              className="mr-2"
              onClick={onClose}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default Toast;
